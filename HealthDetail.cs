﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace EntityModel
{
    [DataContract]
    public class HealthDetail
    {
        [DataMember]
        public string CLIENTID { get; set; }
        [DataMember]
        public string CSCID { get; set; }
        [DataMember]
        public string ORWID { get; set; }
        [DataMember]
        public System.DateTime RECORDEDDATE { get; set; }
        [DataMember]
        public string DATEFORMATTED { get; set; }
        [DataMember]
        public string CD4COUNT { get; set; }
        [DataMember]
        public Nullable<int> ARTREGIMENTYPE { get; set; }        
        [DataMember]
        public Nullable<double> HEIGHT { get; set; }
        [DataMember]
        public Nullable<int> ONATT { get; set; }
        [DataMember]
        public Nullable<double> WEIGHT { get; set; }
        [DataMember]
        public Nullable<double> MUAC { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CREATEDDATE { get; set; }
        [DataMember]
        public Nullable<System.DateTime> UPDATEDDATE { get; set; }
        [DataMember]
        public int PLACE { get; set; }
        [DataMember]
        public int ARTSTATUS { get; set; }
        [DataMember]
        public Nullable<int> PREGNANTLINKTOPPTCT { get; set; }
        [DataMember]
        public string fldSysCRFHId { get; set; }
        [DataMember]
        public string fldCreatedBy { get; set; }
    }
}
